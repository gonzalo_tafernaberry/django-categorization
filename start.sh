#!/bin/bash
echo Starting django-categorization app.
cd /var/www/html/django-categorization/
#gunicorn --bind ec2-52-38-13-82.us-west-2.compute.amazonaws.com:80 gettingstarted.wsgi:application
gunicorn --workers=2 --keyfile  /var/www/html/private.key --certfile /var/www/html/certificate.crt  --bind ec2-52-38-13-82.us-west-2.compute.amazonaws.com:5000 gettingstarted.wsgi:application
